using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameManager Instance;
    public Fireball Fireball;
    public Mushroom Mushroom;
    public int SpawnPositionX = 10;
    public string GameMode = "FirstGameModeScene";
    public float MaxTimeBetweenMushroomSpawn = 10;
    public float MaxTimeBetweenFireballSpawn = 2;
    public int Timer;
    public bool isCoolDownGegantina = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
    }
}
