using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GetPlayerName : MonoBehaviour
{ 
    public TMP_Text TMP_Text;

    void Start()
    {
        PlayerManager playerManager = FindObjectOfType<PlayerManager>();
        TMP_Text.text = playerManager.PlayerName;
    }
}
