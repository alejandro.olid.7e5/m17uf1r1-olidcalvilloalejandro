using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GetFPS : MonoBehaviour
{
    public TMP_Text TMP_Text;
    float time;
    float framesCounter;


    void Start()
    {
        time = 0;
        framesCounter = 0;
    }

    void Update()
    {
        time += Time.deltaTime;
        framesCounter++;
        if (time >= 1)
        {
            TMP_Text.text = "FPS: " + framesCounter.ToString();
            framesCounter = 0;
            time = 0;
        }
    }
}
