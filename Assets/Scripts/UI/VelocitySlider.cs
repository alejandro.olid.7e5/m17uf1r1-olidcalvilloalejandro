using UnityEngine;
using UnityEngine.UI;


public class VelocitySlider : MonoBehaviour
{
    PlayerManager playerManager;
    public Slider slider;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
    }

    public void SetVelocity()
    {
        playerManager.Velocity = slider.value;
    }
}
