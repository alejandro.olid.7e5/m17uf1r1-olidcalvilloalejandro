using UnityEngine;
using UnityEngine.UI;

public class SecondsWaitingSlider : MonoBehaviour
{
    PlayerManager playerManager;
    public Slider slider;

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
    }

    public void SetSecondsWaiting()
    {
        playerManager.SecondsWaiting = (int)slider.value;
    }
}
