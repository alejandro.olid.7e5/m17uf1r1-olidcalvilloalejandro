using UnityEngine;
using UnityEngine.UI;

public class GetPlayerRole : MonoBehaviour
{
    public Image Image;

    void Start()
    {
        PlayerManager playerManager = FindObjectOfType<PlayerManager>();
        Image.sprite = playerManager.RoleChoosenImage;
    }
}
