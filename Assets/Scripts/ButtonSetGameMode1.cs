using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSetGameMode1 : MonoBehaviour
{
    private GameManager gameManager;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

    }

    public void SetGameMode1()
    {
        gameManager.GameMode = "FirstGameModeScene";
    }
}
