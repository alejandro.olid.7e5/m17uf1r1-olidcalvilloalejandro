using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSetGameMode2 : MonoBehaviour
{
    private GameManager gameManager;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

    }

    public void SetGameMode2()
    {
        gameManager.GameMode = "SecondGameModeScene";
    }
}
