using UnityEngine;
public class PrefabSpawn : MonoBehaviour
{
    private Fireball fireball;
    private Mushroom mushroom;
    private GameManager gameManager;
    private int prefabPosition;
    private float counterTime;
    private float timeBetweenSpawns;
    private float maxTimeBetweenSpawns;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

        if (this.gameObject.tag == "MushroomSpawner"){
            maxTimeBetweenSpawns = gameManager.MaxTimeBetweenMushroomSpawn;
            mushroom = gameManager.Mushroom;
        }

        if (this.gameObject.tag == "FireballSpawner")
        {
            maxTimeBetweenSpawns = gameManager.MaxTimeBetweenFireballSpawn;
            fireball = gameManager.Fireball;
        }
    }

    void Update()
    {
        if (counterTime <= 0)
        {
            prefabPosition = Random.Range(-gameManager.SpawnPositionX, gameManager.SpawnPositionX);
            timeBetweenSpawns = Random.Range(1, maxTimeBetweenSpawns);
            Spawn();
            counterTime = timeBetweenSpawns;
        }
        else
            counterTime -= Time.deltaTime;
    }

    void Spawn()
    {
        Vector3 spawnPosition = this.gameObject.transform.position;
        spawnPosition.x += prefabPosition;

        if (this.gameObject.tag == "FireballSpawner")
        {
            Instantiate(fireball, spawnPosition, Quaternion.identity);
        }

        if (this.gameObject.tag == "MushroomSpawner")
        {
            Instantiate(mushroom, spawnPosition, Quaternion.identity);
        }
    }
}
