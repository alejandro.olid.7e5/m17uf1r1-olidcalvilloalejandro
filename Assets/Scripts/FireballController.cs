using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballController : MonoBehaviour
{
    private Fireball fireball;
    private GameManager gameManager;
    private PlayerManager playerManager;
    private float maxTimeBetweenSpawns;
    private float translation;
    private float counterTime;
    private float timeBetweenSpawns;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        playerManager = FindObjectOfType<PlayerManager>();
        maxTimeBetweenSpawns = gameManager.MaxTimeBetweenFireballSpawn;
        fireball = gameManager.Fireball;
    }

    void Update()
    {
        translation = Input.GetAxis("Horizontal");
        translation = translation * playerManager.Velocity * Time.deltaTime;
        transform.Translate(translation, 0, 0);

        if (counterTime <= 0)
        {
            timeBetweenSpawns = Random.Range(1, maxTimeBetweenSpawns);
            Spawn();
            counterTime = timeBetweenSpawns;
        }
        else
            counterTime -= Time.deltaTime;
    }

    void Spawn()
    {
        Instantiate(fireball, this.gameObject.transform.position, Quaternion.identity);
    }
}
