using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PatrolMovement : MonoBehaviour
{
    private PlayerManager playerManager;
    private GameObject player;
    private SpriteRenderer spriteRenderer;
    private Vector3 vHor;
    private Vector3 vectorRaycast;
    private bool isSafe = true;
    private bool areThereGround = true;
    private int direction = -1;
    private bool isDying;
    private float counterDying;
    private float distanceToCheck = 5f;
    private Animator animator;
    public LayerMask groundLayer;
    public LayerMask defaultLayer;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerManager = FindObjectOfType<PlayerManager>();
        spriteRenderer = player.GetComponent<SpriteRenderer>();
        animator = player.GetComponent<Animator>();
    }

    void Update()
    {
        animator.SetFloat("Translation", Math.Abs(playerManager.Velocity));

        checkNextStep();
        checkFireballs();
        Flip();
        patrolMovement();
        LoadScene();
    }

    void Flip()
    {
        if (!areThereGround || !isSafe)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
            direction *= -1;
        }
    }

    void patrolMovement()
    {
        vHor = new Vector3(direction * playerManager.Velocity * Time.deltaTime, 0, 0);
        playerManager.Player.transform.position += vHor;
    }

    void checkNextStep()
    {
        vectorRaycast = new Vector3(player.transform.position.x + direction * 2, player.transform.position.y, player.transform.position.z);
        areThereGround = Physics2D.Raycast(vectorRaycast, Vector2.down, distanceToCheck, groundLayer);
        Debug.DrawRay(vectorRaycast, Vector3.down * distanceToCheck, Color.green);
    }

    void checkFireballs()
    {
        vectorRaycast = new Vector3(player.transform.position.x + direction * 2, player.transform.position.y, player.transform.position.z);
        RaycastHit2D hitInfo = Physics2D.Raycast(vectorRaycast, Vector2.up, distanceToCheck, defaultLayer);
        Debug.DrawRay(vectorRaycast, Vector3.up * distanceToCheck, Color.green);
    }

    private void LoadScene()
    {
        if (isDying)
        {
            counterDying += Time.deltaTime;
            if (counterDying >= 1)
            {
                SceneManager.LoadScene("PlayAgainMenuScene");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Destroyer" || collision.gameObject.tag == "Fireball")
        {
            isDying = true;
            animator.SetBool("isDying", isDying);
        }
    }
}
