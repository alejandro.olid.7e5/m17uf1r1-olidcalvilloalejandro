using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mushroom : MonoBehaviour
{
    private GameManager gameManager;
    private Rigidbody2D rigidbody2D;
    private Animator animator;
    private float counterDestruction = 0;
    public int timeToTake = 5;
    private bool isGrounded = false;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
        animator = this.gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        DestroyThis();
    }

    private void DestroyThis()
    {
        if (isGrounded)
        {
            counterDestruction += Time.deltaTime;
            if (counterDestruction >= timeToTake)
            {
                Destroy(this.gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Destroyer" || collision.gameObject.tag == "Floor")
        {
            rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
            isGrounded = true;
            animator.SetBool("isGrounded", isGrounded);
        }

        if (collision.gameObject.tag == "Player")
        {
            animator.SetBool("isTaken", true);
            var gameObjectList = SceneManager.GetActiveScene().GetRootGameObjects();
            foreach (GameObject obj in gameObjectList)
            {
                if (obj.CompareTag("Fireball"))
                {
                    obj.GetComponent<Fireball>().Gegantina();
                }
            }
            Destroy(this.gameObject);
        }
    }
}
