using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fireball : MonoBehaviour
{
    public float counterGigantina = 0;
    public float counterDestruction = 0;
    private bool onCoolDown = false;
    private bool isDestroyed = false;
    private Animator animator;
    private Rigidbody2D rigidbody2D;

    void Start()
    {
        animator = this.gameObject.GetComponent<Animator>();
        rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        CoolDownGegantina(onCoolDown);
        DestroyThis();
    }

    private void CoolDownGegantina(bool onCoolDown)
    {
        if (onCoolDown)
        {
            counterGigantina += Time.deltaTime;
            if (counterGigantina >= 5)
            {
                onCoolDown = false;
                counterGigantina = 0;
                this.transform.localScale = Vector3.one;
            }
        }
    }

    private void DestroyThis()
    {
        if (isDestroyed)
        {
            rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
            counterDestruction += Time.deltaTime;
            if (counterDestruction >= 1)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public void Gegantina()
    {
        this.transform.localScale *= 2;
        onCoolDown = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Destroyer" || collision.gameObject.tag == "Floor")
        {
            isDestroyed = true;
            animator.SetBool("isDestroyed", isDestroyed);
        }
    }
}
