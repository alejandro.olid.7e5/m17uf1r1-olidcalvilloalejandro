 using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private GameObject player;
    private PlayerManager playerManager;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rigidbody2D;
    private Animator animator;
    public LayerMask groundLayer;
    private bool isDying;
    private int playerOrientation;
    private float translation;
    private float counterDying;
    public int jumpAmount = 3;
    public float gravityScale = 10;
    public float fallingGravityScale = 40;
    public float distanceToCheck = 1f;
    public bool isGrounded;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerManager = FindObjectOfType<PlayerManager>();
        spriteRenderer = player.GetComponent<SpriteRenderer>();
        rigidbody2D = player.GetComponent<Rigidbody2D>();
        animator = player.GetComponent<Animator>();
    }

    void Update()
    {
        translation = Input.GetAxis("Horizontal");
        //Pasamos el input del movimiento sin modificar para hacer una buena comprobacion
        animator.SetFloat("Translation", Math.Abs(translation));
        //Calculamos el moviento teniendo en cuenta: Velocidad, peso y movimiento
        translation = translation * playerManager.Velocity * (50 / playerManager.Weight) * Time.deltaTime;
        transform.Translate(translation, 0, 0);

        SetOrientation();
        jump();
        checkIsGrounded();
        animator.SetBool("IsGrounded", isGrounded);
        animator.SetFloat("VelocityY", rigidbody2D.velocity.y);

        LoadScene();
    }

    private void SetOrientation()
    {
        if (translation > 0)
        {
            playerOrientation = 1;
            spriteRenderer.flipX = false;
        }

        if (translation < 0)
        {
            playerOrientation = -1;
            spriteRenderer.flipX = true;
        }
    }

    void checkIsGrounded()
    {
        isGrounded = Physics2D.Raycast(player.transform.position, Vector2.down, distanceToCheck, groundLayer);
        Debug.DrawRay(player.transform.position, Vector3.down * distanceToCheck, Color.green);
    }

    void jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rigidbody2D.AddForce(new Vector2(playerOrientation, 10) * jumpAmount * 10, ForceMode2D.Impulse);
        }
        if (rigidbody2D.velocity.y >= 0)
        {
            rigidbody2D.gravityScale = gravityScale;
        }
        else if (rigidbody2D.velocity.y < 0)
        {
            rigidbody2D.gravityScale = fallingGravityScale;
        }
    }

    private void LoadScene()
    {
        if (isDying)
        {
            counterDying += Time.deltaTime;
            if (counterDying >= 1)
            {
                SceneManager.LoadScene("PlayAgainMenuScene");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Destroyer" || collision.gameObject.tag == "Fireball")
        {
            isDying = true;
            animator.SetBool("isDying", isDying);
        }
    }
}
