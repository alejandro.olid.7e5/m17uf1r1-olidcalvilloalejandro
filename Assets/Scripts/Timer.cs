using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    private GameManager gameManager;
    private float counter = 0;
    private TMP_Text textComponent;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        textComponent = this.gameObject.GetComponent<TMP_Text>();
    }

    void Update()
    {
        if (counter >= 1){
            counter = 0;
            gameManager.Timer--;
            textComponent.text = gameManager.Timer.ToString();
        }
        if (gameManager.Timer <= 0){
            SceneManager.LoadScene("PlayAgainMenuScene");
        }
        counter += Time.deltaTime;
    }
}
